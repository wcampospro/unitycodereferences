﻿using UnityEngine;

public class AreaScope : MonoBehaviour
{

    [SerializeField]
    private float _ZoomFactor = 1f;

    [SerializeField]
    private Transform[] _Positions;

    [SerializeField]
    private float _MinCameraDist = 3f;


    private Camera _Cam;


    private void Awake()
    {
        _Cam = GetComponent<Camera>();
        var v3 = new Vector3(1f, 2f, 3f);

        v3 = v3.With(y: 5f);
        print(v3);
    }

    private void LateUpdate()
    {
        float aspectRatio = (float)_Cam.scaledPixelWidth/(float)_Cam.scaledPixelHeight;
        Vector3 minPos = Vector3.one.With(y: 0f) * float.MaxValue;
        Vector3 maxPos = Vector3.one.With(y: 0f) * -float.MaxValue;
        for (int i = 0; i < _Positions.Length; i++)
        {
            if (_Positions[i].position.x < minPos.x) minPos.x = _Positions[i].position.x;
            if (_Positions[i].position.z < minPos.z) minPos.z = _Positions[i].position.z;
            if (_Positions[i].position.x > maxPos.x) maxPos.x = _Positions[i].position.x;
            if (_Positions[i].position.z > maxPos.z) maxPos.z = _Positions[i].position.z;
        }
        Vector3 avgPos = (minPos + maxPos) / 2f;
        float xDist = maxPos.x - minPos.x;
        float zDist = maxPos.z - minPos.z;

        var diagonal = maxPos - minPos;
        diagonal.z *= aspectRatio;
        avgPos.y = diagonal.magnitude * _ZoomFactor;
        avgPos.y = avgPos.y < _MinCameraDist ? _MinCameraDist : avgPos.y;

        transform.position = avgPos;
    }
}

public static class VectorExtensions
{
    public static Vector3 With(this Vector3 v3, float? x = null, float? y = null, float? z = null) => new Vector3(x??v3.x, y??v3.y, z??v3.z);
    public static Vector2 GetXZ(this Vector3 v3) => new Vector2(v3.x, v3.z);
}
