﻿using UnityEngine;

public class Voronoi_Drawer : MonoBehaviour
{

    [SerializeField]
    private Transform _T1 = null;

    [SerializeField]
    private Transform _T2 = null;

    [SerializeField]
    private float _PerpendicularLength = 10f;

    [SerializeField]
    private Color _Color = Color.white;

    private Vector3 _AvgPos;
    private Vector3 _Dir;
    private Vector3 _Perpendicular;

    private void Update()
    {
        _AvgPos = (_T1.position + _T2.position)/2f;
        _Dir = (_T1.position - _T2.position);
        _Perpendicular = Vector3.Cross(Vector3.up, _Dir.normalized);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.gray;
        Gizmos.DrawLine(_T1.position, _T2.position);

        var p1 = _AvgPos - _Perpendicular * _PerpendicularLength / 2f;
        var p2 = _AvgPos + _Perpendicular * _PerpendicularLength / 2f;
        Gizmos.color = _Color;
        Gizmos.DrawLine(p1, p2);
    }

}
