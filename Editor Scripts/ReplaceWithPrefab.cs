using System;
using UnityEngine;
using UnityEditor;

public class ReplaceWithPrefab : EditorWindow
{
    [SerializeField]
    private GameObject _prefab;

    [MenuItem("Tools/Replace With Prefab")]
    static public void CreateReplaceWithPrefab()
    {
        GetWindow<ReplaceWithPrefab>();
    }

    private void OnGUI()
    {
        _prefab = (GameObject) EditorGUILayout.ObjectField("Prefab", _prefab, typeof(GameObject), false);
        var selectedObjects = Selection.gameObjects ?? Array.Empty<GameObject>();

        if (_prefab == null || selectedObjects.Length <= 0)
        {
            GUI.enabled = false;
            GUILayout.Button("Replace");
            return;
        }

        GUI.enabled = true;
        if (GUILayout.Button("Replace"))
        {
            foreach (var objectToReplace in selectedObjects)
            {
                var newObject = Instantiate(_prefab,
                                            objectToReplace.transform.localPosition,
                                            objectToReplace.transform.localRotation,
                                            objectToReplace.transform.parent);
                newObject.name = _prefab.name;
                Undo.RegisterCreatedObjectUndo(newObject, "Replace With Prefabs");
                newObject.transform.localScale = objectToReplace.transform.localScale;
                newObject.transform.SetSiblingIndex(objectToReplace.transform.GetSiblingIndex());

                Undo.DestroyObjectImmediate(objectToReplace);
            }
        }

        GUI.enabled = false;
        EditorGUILayout.LabelField("Selection count: " + Selection.objects.Length);
    }
}
