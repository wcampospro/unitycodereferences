﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CreateCharacterWizard : ScriptableWizard
{
    public Texture2D texture;
    public string daName;
    public float moveSpeed;

    [MenuItem("Tools/Create Character Wizard...")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<CreateCharacterWizard>("Create Character", "Create New", "Update Selected");

    }

    private void OnWizardCreate()
    {
        GameObject charGObj = new GameObject();
        Character charComp = charGObj.AddComponent<Character>();
        charComp.portrait = texture;
        charComp.daName = name;
        charComp.moveSpeed = moveSpeed;
    }

    private void OnWizardOtherButton()
    {
        if (Selection.activeTransform == null)
            return;

        Character comp = Selection.activeTransform.GetComponent<Character>();
        if (comp == null)
            return;


        comp.portrait = texture;
        comp.daName = name;
        comp.moveSpeed = moveSpeed;
    }

    private void OnWizardUpdate()
    {
        helpString = "Enter Character Details";
    }

}
