using UnityEngine;
using UnityEditor;

public class MenuItems : MonoBehaviour
{

    [MenuItem("Tools/SnapDown _END")]
    public static void SnapDown()
    {
        Transform tr = Selection.activeTransform;
        RaycastHit hit;
        if (Physics.Raycast(tr.position, Vector3.down, out hit))
            tr.position = tr.position.With(y: hit.point.y + tr.GetComponent<Renderer>().bounds.size.y*0.5f);
    }

    [MenuItem("Tools/SnapAlignDown %END")]
    public static void SnapAlignDown()
    {
        Transform tr = Selection.activeTransform;
        RaycastHit hit;
        if (Physics.Raycast(tr.position, Vector3.down, out hit))
        {
            tr.position = tr.position.With(y: hit.point.y + tr.GetComponent<Renderer>().bounds.size.y * 0.5f);
            tr.up = hit.normal;
        }
    }

}
