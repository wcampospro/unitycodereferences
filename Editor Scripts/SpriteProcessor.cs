﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SpriteProcessor : AssetPostprocessor
{
    void OnPostProcesstexture(Texture2D tex)
    {
        string lowerCaseAssetPath = assetPath.ToLower();
        bool isInSpritesDir = lowerCaseAssetPath.IndexOf("/sprites/") != -1;

        if (isInSpritesDir)
        {
            TextureImporter texImporter = (TextureImporter)assetImporter;
            texImporter.textureType = TextureImporterType.Sprite;
        }
    }
}
