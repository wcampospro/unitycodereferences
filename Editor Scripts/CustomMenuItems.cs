﻿using UnityEngine;
using UnityEditor;

//***********************************************************************************************************************************************
public class CustomMenuItems : Editor
{
    //Snaps selected objects to ground
    [MenuItem("Tools/SnapToGround %END")]
    public static void SnapToGround()
    {
        foreach (Transform t in Selection.transforms)
        {
            RaycastHit rayhit;
            if (Physics.Raycast(t.position, Vector3.down, out rayhit))
            {
                Vector3 offset = Vector3.zero;
                MeshRenderer renderer = t.GetComponentInChildren<MeshRenderer>();
                if (renderer != null)
                {
                    if (renderer.bounds.center.y - renderer.bounds.extents.y < t.position.y)
                    {
                        offset = new Vector3(0, renderer.bounds.extents.y, 0);
                    }
                }
                t.position = rayhit.point + offset;
            }
        }
    }

    //Snaps to ground and Alignts to Normal
    [MenuItem("Tools/SnapAndAlign &END")]
    public static void SnapAndAlign()
    {
        foreach (Transform t in Selection.transforms)
        {
            RaycastHit rayhit;
            if (Physics.Raycast(t.position, Vector3.down, out rayhit))
            {
                Vector3 offset = Vector3.zero;
                MeshRenderer renderer = t.GetComponentInChildren<MeshRenderer>();
                if (renderer != null)
                {
                    if (renderer.bounds.center.y - renderer.bounds.extents.y < t.position.y)
                    {
                        offset = new Vector3(0, renderer.bounds.extents.y, 0);
                    }
                }
                t.position = rayhit.point + offset;
                t.up = rayhit.normal;
            }
        }
    }
}


//***********************************************************************************************************************************************
//Selects all objects in the scene with a specific Tag
public class SelectAllOfTag : ScriptableWizard
{
    public string searchTag = "";

    [MenuItem("Tools/Select All Of Tag... %&t")]
    static void SelectAllOfTagWizard()
    {
        ScriptableWizard.DisplayWizard<SelectAllOfTag>("Select All Of Tag...", "Make Selection");
    }

    private void OnWizardCreate()
    {
        GameObject[] gObjs = GameObject.FindGameObjectsWithTag(searchTag);
        Selection.objects = gObjs;
    }
}


//***********************************************************************************************************************************************
[CustomEditor(typeof(LevelScript))]
public class LevelScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        LevelScript myLevelScript = (LevelScript)target;
        myLevelScript.experience = EditorGUILayout.IntField("Experience", myLevelScript.experience);
        EditorGUILayout.LabelField("Level", myLevelScript.level.ToString());
    }
}