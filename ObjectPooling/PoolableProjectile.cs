﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PoolableProjectile : PoolableObject {

    public float projectileSpeed;
    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    protected override void OnEnable_PO()
    {
        rb.velocity = transform.forward * projectileSpeed;
    }

    protected override void OnDisable_PO() { }
}
