﻿using UnityEngine;

public class GameManager : Singleton<GameManager> {

    public GameObject NOP_Bullet;
    public PoolableObject PO_Bullet_1;

    private void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            Instantiate<GameObject>(NOP_Bullet, Vector3.zero, Quaternion.identity);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            PoolableObject clone = PoolManager.instance.GetObjectFromPool(PO_Bullet_1);
            clone.transform.position = new Vector3(-2, 0, 0);
            clone.gameObject.SetActive(true);
        }

    }

}
