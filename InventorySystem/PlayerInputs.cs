﻿using System.Collections.Generic;
using UnityEngine;


public class PlayerInputs : MonoBehaviour {

    private List<SO_Item> items;

    private void Start()
    {
        items = InventoryManager.instance.availableItems.Items;
    }

    void Update () {

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (InventoryManager.instance.removeItem(items[0]))
                Instantiate<GameObject>(items[0].ActionableItem, transform.position, Quaternion.identity);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (InventoryManager.instance.removeItem(items[1]))
                Instantiate<GameObject>(items[1].ActionableItem, transform.position, Quaternion.identity);
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (InventoryManager.instance.removeItem(items[2]))
                Instantiate<GameObject>(items[2].ActionableItem, transform.position, Quaternion.identity);
        }

    }
}
