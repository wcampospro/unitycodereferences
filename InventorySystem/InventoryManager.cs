﻿using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour {

    #region Singleton
    public static InventoryManager instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            return;
        }
        instance = this;
    }
    #endregion

    #region Events
    public delegate void InventoryModified(SO_Item item, int count);
    public static event InventoryModified OnItemAdded;
    public static event InventoryModified OnItemRemoved;
    #endregion

    public SO_Inventory availableItems;
    private Dictionary<SO_Item, int> playerInventory = new Dictionary<SO_Item, int>();

    public bool addItem(SO_Item itemToAdd)
    {
        //If the item to add is Null, then cant add any item
        if (itemToAdd == null)
            return false;

        //If my disctionary does not contain the item, then add 1
        int count = 0;
        if (!playerInventory.TryGetValue(itemToAdd, out count))
        {
            playerInventory.Add(itemToAdd, 1);
            if (OnItemAdded != null)
                OnItemAdded(itemToAdd, 1);
            return true;
        }

        //if i already have the maxamount or more then dont add
        if (count >= itemToAdd.MaxAmount)
            return false;

        //Else: increment my itemtoadd count by 1
        playerInventory[itemToAdd]++;
        if (OnItemAdded != null)
            OnItemAdded(itemToAdd, playerInventory[itemToAdd]);
        return true;
    }

    public bool removeItem(SO_Item itemToRemove)
    {
        int count = 0;
        if (playerInventory.TryGetValue(itemToRemove, out count))
        {
            if (count <= 1)
            {
                playerInventory.Remove(itemToRemove);
                if (count < 1)
                {
                    return false;
                }
                else
                {
                    if (OnItemRemoved != null)
                        OnItemRemoved(itemToRemove, 0);
                    return true;
                }
            }

            playerInventory[itemToRemove]--;
            if (OnItemRemoved != null)
                OnItemRemoved(itemToRemove, playerInventory[itemToRemove]);
            return true;
        }
        return false;
    }

}
