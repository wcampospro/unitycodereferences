// Low level API Echo_Server
// Same system responsetime down to ~5ms (roundtrip)
// for both Reliable and Unreliable messages

using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class LLServer : MonoBehaviour
{
    public int ServerPort = 8000;
    public ushort BufferSize = 1024;
    public byte threadPoolSize = 4;
    public ushort maxPacketSize = 2048;
    public uint messageDelayMS = 0;

    private int ReliableChannel = 0;
    private int UnreliableChannel = 0;
    private int SocketID = 0;
    private bool isRunning = false;
    private WaitForSecondsRealtime coroutineDelay;

    //-------------------------------------------------------------------------------------
    void Start()
    {
        StartServer();
    }

    //-------------------------------------------------------------------------------------
    void StartServer()
    {
        GlobalConfig globalFonfig = new GlobalConfig();
        globalFonfig.ThreadPoolSize = threadPoolSize;
        globalFonfig.MaxPacketSize = maxPacketSize;
        NetworkTransport.Init(globalFonfig);

        ConnectionConfig connectionConfig = new ConnectionConfig();
        connectionConfig.SendDelay = 0;
        connectionConfig.AckDelay = 0;
        connectionConfig.MinUpdateTimeout = messageDelayMS == 0 ? 1 : messageDelayMS;
        ReliableChannel = connectionConfig.AddChannel(QosType.Reliable);
        UnreliableChannel = connectionConfig.AddChannel(QosType.Unreliable);

        HostTopology topology = new HostTopology(connectionConfig, 16);
        SocketID = NetworkTransport.AddHost(topology, ServerPort);

        isRunning = true;
        coroutineDelay = new WaitForSecondsRealtime(0.001f);
        StartCoroutine(Receiver());
        //EZThread.BeginThread(Receiver);
    }

    /// <summary>
    /// Runs in it's own thread
    /// </summary>
    IEnumerator Receiver()
    {
        int recSocketId;
        int recConnectionId;
        int recChannelId;
        int dataSize;
        byte error;

        int messageCount = 0;
        while (isRunning)
        {
            byte[] recBuffer = new byte[BufferSize];
            NetworkEventType NEType = NetworkTransport.Receive(out recSocketId, out recConnectionId, out recChannelId, recBuffer, BufferSize, out dataSize, out error);
            switch (NEType)
            {
                case NetworkEventType.Nothing:
                    print("Messages this frame: " + messageCount);
                    messageCount = 0;
                    yield return null; //VSync off and running a headless server in -batchmode is ideal
                    break;
                case NetworkEventType.DataEvent:
                    OnDataEvent(recConnectionId, recChannelId, recBuffer, dataSize);
                    ++messageCount;
                    break;
                case NetworkEventType.ConnectEvent:
                    OnConnectEvent(recConnectionId);
                    ++messageCount;
                    break;
                case NetworkEventType.DisconnectEvent:
                    OnDisconnectEvent(recConnectionId);
                    ++messageCount;
                    break;
                default:
                    break;
            }
        }
    }

    //-------------------------------------------------------------------------------------
    void OnConnectEvent(int connectionId)
    {
        Debug.Log("User Connected: " + connectionId);
    }

    //-------------------------------------------------------------------------------------
    void OnDataEvent(int recConnectionId, int recChannelId, byte[] data, int dataSize)
    {
        //Just Echo back
        byte bErr = 0;
        NetworkTransport.Send(SocketID, recConnectionId, recChannelId, data, dataSize, out bErr);
    }

    //-------------------------------------------------------------------------------------
    void OnDisconnectEvent(int connectionId)
    {
        Debug.Log("User Disonnected: " + connectionId);
    }

}
