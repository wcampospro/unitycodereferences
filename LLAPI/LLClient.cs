// Low level API Client for EchoServer
// Same system responsetime down to ~5ms (roundtrip)
// for both Reliable and Unreliable messages

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class LLClient : MonoBehaviour
{
    public string ServerAddress = "127.0.0.1";
    public int ServerPort = 8000;
    public int LocalPort = 0;
    public ushort BufferSize = 1024;
    public byte threadPoolSize = 4;
    public ushort maxPacketSize = 2048;
    public uint messageDelayMS = 0;

    private int ReliableChannel = 0;
    private int UnreliableChannel = 0;
    private int SocketID = 0;
    private bool isRunning = false;
    private int ServerConnectionID = 0;

    private DateTime before;
    private double ts;
    private double latencyTotal = 0;
    private long latencyCount = 0;
    private WaitForSecondsRealtime coroutineDelay;

    //-------------------------------------------------------------------------------------
    void Start()
    {
        ConnectToServer();
        InvokeRepeating("Pinger", 2, 1);
    }

    //-------------------------------------------------------------------------------------
    void ConnectToServer()
    {
        GlobalConfig globalFonfig = new GlobalConfig();
        globalFonfig.ThreadPoolSize = threadPoolSize;
        globalFonfig.MaxPacketSize = maxPacketSize;
        NetworkTransport.Init(globalFonfig);

        ConnectionConfig connectionConfig = new ConnectionConfig();
        connectionConfig.SendDelay = messageDelayMS;
        connectionConfig.AckDelay = messageDelayMS;
        connectionConfig.MinUpdateTimeout = messageDelayMS == 0 ? 1 : messageDelayMS;

        ReliableChannel = connectionConfig.AddChannel(QosType.Reliable);
        UnreliableChannel = connectionConfig.AddChannel(QosType.Unreliable);

        HostTopology topology = new HostTopology(connectionConfig, 16);
        SocketID = NetworkTransport.AddHost(topology, LocalPort);

        byte bErr = 0;
        ServerConnectionID = NetworkTransport.Connect(SocketID, ServerAddress, ServerPort, 0, out bErr);

        isRunning = true;
        coroutineDelay = new WaitForSecondsRealtime(0.001f);
        StartCoroutine(Receiver());
    }

    //-------------------------------------------------------------------------------------
    byte[] bArr;
    string sPing = "EMPTY";
    void Pinger()
    {
        byte bErr = 0;
        bArr = BitConverter.GetBytes(DateTime.Now.Ticks);
        NetworkTransport.Send(SocketID, ServerConnectionID, ReliableChannel, bArr, bArr.Length, out bErr);
    }

    //-------------------------------------------------------------------------------------
    IEnumerator Receiver()
    {
        int recSocketId;
        int recConnectionId;
        int recChannelId;
        int dataSize;
        byte error;

        int messageCount = 0;
        while (isRunning)
        {
            byte[] recBuffer = new byte[BufferSize];
            NetworkEventType NEType = NetworkTransport.Receive(out recSocketId, out recConnectionId, out recChannelId, recBuffer, BufferSize, out dataSize, out error);
            switch (NEType)
            {
                case NetworkEventType.Nothing:
                    print("Messages this frame: " + messageCount);
                    messageCount = 0;
                    yield return null; //frame rate dependant
                    break;
                case NetworkEventType.DataEvent:
                    OnDataEvent(recConnectionId, recChannelId, recBuffer, dataSize);
                    ++messageCount;
                    break;
                case NetworkEventType.ConnectEvent:
                    OnConnectEvent(recConnectionId);
                    ++messageCount;
                    break;
                case NetworkEventType.DisconnectEvent:
                    OnDisconnectEvent(recConnectionId);
                    ++messageCount;
                    break;
                default:
                    break;
            }
        }
    }

    //-------------------------------------------------------------------------------------
    void OnConnectEvent(int connectionId)
    {
        Debug.Log("Connected to server: " + connectionId);
    }

    //-------------------------------------------------------------------------------------
    void OnDataEvent(int recConnectionId, int recChannelId, byte[] data, int dataSize)
    {
        ts = DateTime.Now.Subtract(DateTime.FromBinary(BitConverter.ToInt64(data, 0))).TotalMilliseconds;
        latencyTotal += ts;
        ++latencyCount;
        sPing = (float)(latencyTotal / latencyCount) + " - " + ts + " ms";
    }

    //-------------------------------------------------------------------------------------
    void OnDisconnectEvent(int connectionId)
    {
        Debug.Log("User Disonnected: " + connectionId);
    }

    //-------------------------------------------------------------------------------------
    private void OnGUI()
    {
        GUI.Label(new Rect(768, 5, 256, 32), sPing);
    }

}
