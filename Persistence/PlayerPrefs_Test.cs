﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPrefs_Test : MonoBehaviour {
    public string str;

    private void OnEnable()
    {
        str = PlayerPrefs.GetString("StringKey");
        PlayerPrefs.SetInt("IntKey", i+1);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //SceneManager.LoadScene("ObjectPooling");
            SceneManager.LoadScene("Persistence");
        }
    }

    private void OnDisable()
    {
        PlayerPrefs.SetString("StringKey", str);
    }

}
