using UnityEngine;

// This is for educational purposes
// Professionaly speaking you should try as hard as possible to stay away from Singletons
// Singletons Promote Code Coupling, making everything under them a static global state

public abstract class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    public static T instance;
    private static bool _DDOL;

    protected void Awake()
    {
        if (instance == null)
        {
            instance = GetComponent<T>();
            if(_DDOL)
            {
                DontDestroyOnLoad(instance.gameObject);
            }
            SingletonAwake();
            return;
        }

        Destroy(this);
    }

    public static void Require(bool ddol = false)
    {
        if(instance == null)
        {
            _DDOL = ddol;
            var tmp = new GameObject();
            tmp.name = "Singleton_"+ nameof(T);
            tmp.AddComponent<T>();
        }
        else if(ddol)
        {
            _DDOL = ddol;
            DontDestroyOnLoad(instance.gameObject);
        }
    }

    public abstract void SingletonAwake();
}
