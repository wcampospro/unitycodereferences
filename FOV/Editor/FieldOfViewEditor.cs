﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FieldOfView))]
public class FieldOfViewEditor : Editor
{
    private void OnSceneGUI()
    {
        if (!(target is FieldOfView fov))
        {
            return;
        }

        var transform = fov.transform;
        var position = transform.position;
        (float viewRadius, float viewAngle) = fov.GetRadiusAngle();
        var vAngle = viewAngle / 2f;
        
        Vector3 vieAngleA = transform.DirFromAngle(-vAngle, false);
        Vector3 vieAngleB = transform.DirFromAngle(vAngle, false);

        Handles.color = Color.white;
        Handles.DrawWireArc(position, Vector3.up, vieAngleA, viewAngle, viewRadius);

        Handles.DrawLine(position, position + vieAngleA * viewRadius);
        Handles.DrawLine(position, position + vieAngleB * viewRadius);

        Handles.color = Color.red;
        foreach (var item in fov.VisibleTargets)
        {
            Handles.DrawLine(position, item.position);
        }
    }
}
