﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Test MonoBehaviour to show how FOV works
/// </summary>
public class FieldOfView : MonoBehaviour
{
    [SerializeField]
    private float _ViewRadius = 3f;

    [SerializeField]
    [Range(0f, 360f)]
    private float _ViewAngle = 90f;

    [SerializeField]
    private LayerMask _TargetMask;

    [SerializeField]
    private LayerMask _ObstacleMask;

    
    public List<Transform> VisibleTargets { get; private set; } = new List<Transform>();
    

    public (float viewRadius, float viewAngle) GetRadiusAngle()
    {
        return (_ViewRadius, _ViewAngle);
    }

    private void OnDrawGizmosSelected()
    {
        VisibleTargets = transform.GetTargetsInFoV(_ViewRadius, _ViewAngle, _TargetMask, _ObstacleMask);
    }

}
