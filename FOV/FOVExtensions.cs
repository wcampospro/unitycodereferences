﻿using System;
using System.Collections.Generic;
using UnityEngine;

//Put into Transform extension methods
public static class FOVExtensions
{
    public static List<Transform> GetTargetsInFoV(this Transform transform,
                                                     float viewRadius,
                                                     float viewAngle,
                                                     LayerMask targetMask,
                                                     LayerMask obstacleMask
    )
    {
        var visibleTargets = new List<Transform>();
        var targetsInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);

        for (int i = 0; i < targetsInViewRadius.Length; i++)
        {
            var target = targetsInViewRadius[i].transform;
            var dirToTarget = (target.position - transform.position).normalized;
            
            if (Vector3.Angle(transform.forward, dirToTarget) > viewAngle / 2f)
            {
                continue;
            }

            float dist = Vector3.Distance(transform.position, target.position);
            if (Physics.Raycast(transform.position, dirToTarget, dist, obstacleMask) == false)
            {
                visibleTargets.Add(target);
            }
        }
        Debug.Log(visibleTargets.Count);
        return visibleTargets;
    }

    public static Vector3 DirFromAngle(this Transform transform, float angleInDegrees, bool globalAngle)
    {
        if (globalAngle == false)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
    
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0f, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}
