﻿using StateMachineStuff;
using UnityEngine;

public class State_1 : State
{
    public override void EnterState<O>(O owner)
    {
        Debug.Log("Enter State_1 " + owner.ToString());
    }

    public override void ExitState<O>(O owner)
    {
        Debug.Log("Exit State_1 " + owner.ToString());
    }

    public override void UpdateState<O>(O owner)
    {

    }
}
