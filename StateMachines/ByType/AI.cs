﻿using StateMachineStuff;
using UnityEngine;

public class AI : MonoBehaviour
{
    public StateMachine<AI> stateMachine;

    protected void Awake()
    {
        stateMachine = new StateMachine<AI>(this);
        stateMachine.ChangeState<State_1>();
        Invoke("ChangeToS2", 3);
        Invoke("ChangeToS1", 6);
    }

    private void Update()
    {
        stateMachine.UpdateStates();
    }

    void ChangeToS1()
    {
        stateMachine.ChangeState<State_1>();
    }

    void ChangeToS2()
    {
        stateMachine.ChangeState<State_2>();
    }

}
