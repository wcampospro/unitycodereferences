using System;
using System.Collections.Generic;

namespace StateMachine_NS
{
    public class StateMachine<O>
    {
        public O Owner { get; private set; }
        public State currentState { get; private set; }

        public StateMachine(O owner)
        {
            currentState = null;
            Owner = owner;
        }

        public void ChangeState<T>() where T : State, new()
        {
            if (typeof(T) == currentState.GetType())
            {
                currentState.ExitState(Owner);
                currentState.EnterState(Owner);
                return;
            }

            T newState = new T();
            if (currentState != null)
                currentState.ExitState(Owner);

            currentState = newState;
            currentState.EnterState(Owner);
        }

        public void UpdateState()
        {
            currentState.UpdateState(Owner);
        }
        
    }

    public class StateMachine_D<O>
    {
        public O Owner { get; private set; }
        public State currentState { get; private set; }

        private Dictionary<Type, State> states;

        public StateMachine_D(O owner)
        {
            states = new Dictionary<Type, State>();
            currentState = null;
            Owner = owner;
        }

        public void ChangeState<T>() where T : State, new()
        {
            if (typeof(T) == currentState.GetType())
            {
                currentState.ExitState(Owner);
                currentState.EnterState(Owner);
                return;
            }

            State newSTate;
            if (!states.TryGetValue(typeof(T), out newSTate))
            {
                newSTate = new T();
                states.Add(typeof(T), newSTate);
            }

            if (currentState != null)
                currentState.ExitState(Owner);

            currentState = newSTate;
            currentState.EnterState(Owner);
        }

        public void UpdateStates()
        {
            currentState.UpdateState(Owner);
        }

    }

    public abstract class State
    {
        public abstract void EnterState<O>(O owner);
        public abstract void UpdateState<O>(O owner);
        public abstract void ExitState<O>(O owner);
    }

}
