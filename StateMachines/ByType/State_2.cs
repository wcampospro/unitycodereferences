﻿using StateMachineStuff;
using UnityEngine;

public class State_2 : State
{
    public override void EnterState<O>(O owner)
    {
        Debug.Log("Enter State_2 " + owner.ToString());
    }

    public override void ExitState<O>(O owner)
    {
        Debug.Log("Exit State_2 " + owner.ToString());
    }

    public override void UpdateState<O>(O owner)
    {
        Debug.Log(owner.ToString() + " @ " + Time.time);
    }
}