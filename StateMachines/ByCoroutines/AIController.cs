using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour {

    protected IEnumerator currentState;

    protected void Awake()
    {
        base.Awake();
    }

    private void Start()
    {
        SetState(OnIdle());
    }

    protected void SetState(IEnumerator newState)
    {
        if (currentState != null)
            StopCoroutine(currentState);

        currentState = newState;
        StartCoroutine(currentState);
    }

    private IEnumerator OnIdle()
    {
        yield return null;
    }

    private IEnumerator OnMovingToWaypoint()
    {
        yield return null;
    }

    private IEnumerator OnMovingToEnemy()
    {
        yield return null;
    }

    private IEnumerator OnAttacking()
    {
        yield return null;
    }

}
