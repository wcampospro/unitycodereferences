﻿using UnityEngine;
using System.Linq;

public class S_Idle : State<CubeBoy>
{
    private string _Tag;

    public S_Idle(CubeBoy owner, string tag)
    {
        _Owner = owner;
        _Tag = tag;
    }

    public override void Enter() { Debug.Log("Enter S_Idle State: " + _Tag); }

    public override void Exit() { }

    public void Update_NoLinq()
    {
        var hitObjects = Physics.OverlapSphere(_Owner.transform.position, _Owner.foodRadius, _Owner.maskToSearch);
        foreach (var obj in hitObjects)
        {
            if (obj.CompareTag(_Tag))
            {
                _Owner.currentTarget = obj.transform;
                _Owner.ChangeState(new S_MovingToFood(_Owner));
                return;
            }
        }
    }

    public override void Update()
    {
        var closest = Physics.OverlapSphere(_Owner.transform.position, _Owner.foodRadius, _Owner.maskToSearch)
            .Where(t => t.CompareTag(_Tag))
            .OrderBy(t => Vector3.SqrMagnitude(_Owner.transform.position - t.transform.position))
            .FirstOrDefault();

        if (closest == null) return;

        _Owner.currentTarget = closest.transform;
        _Owner.ChangeState(new S_MovingToFood(_Owner));
    }
}
