﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_MovingToFood : State<CubeBoy>
{

    public S_MovingToFood(CubeBoy owner)
    {
        _Owner = owner;
    }

    public override void Enter() { Debug.Log("Enter S_MovingToFood State: ") ; }

    public override void Exit() { }

    public override void Update()
    {
        if (_Owner.currentTarget == null || Vector3.SqrMagnitude(_Owner.transform.position - _Owner.currentTarget.position) > (_Owner.foodRadius * _Owner.foodRadius))
        {
            _Owner.ChangeState(new S_Idle(_Owner, "Food"));
            return;
        }

        _Owner.SetDestination(_Owner.currentTarget.position);
        if (Vector3.SqrMagnitude(_Owner.transform.position - _Owner.currentTarget.position) <= 1)
        {
            _Owner.ChangeState(new S_Idle(_Owner, "Food"));
        }
    }

}
