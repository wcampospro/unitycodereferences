﻿using UnityEngine;

//Here we have a U type that limits the types we can find
//We also define the type of state to State<CubeBoy> in this case
public class S_SearchFor : State<CubeBoy>
{
    private float _Radius;
    private string _Tag;

    public S_SearchFor(CubeBoy owner, float radius, string tag)
    {
        _Owner = owner;
        _Radius = radius;
        _Tag = tag;
    }

    public override void Enter() { Debug.Log("Enter SearchFor State: "+ _Tag); }

    public override void Exit() { }

    public override void Update()
    {
        var hitObjects = Physics.OverlapSphere(_Owner.transform.position, _Radius);
        foreach (var obj in hitObjects)
        {
            if (obj.CompareTag(_Tag))
            {
                //_Owner.ChangeState(new S_SearchFor<T_Food>(_Owner, _Radius));
                _Owner.SetDestination(obj.transform.position);
                return;
            }
        }
    }
}
