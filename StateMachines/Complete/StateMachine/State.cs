﻿public abstract class State<T>
{
    protected T _Owner;
	
	protected State(T owner)
	{
		_Owner = owner;		
	}

    public abstract void Enter();

    public abstract void Update();

    public abstract void Exit();
}
