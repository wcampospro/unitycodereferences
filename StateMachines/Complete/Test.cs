﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public GameObject[] objs;


    private void Update()
    {
        RunTags();
        RunComponents();
    }


    float RunTags()
    {
        int food = 0;
        int enemy = 0;
        float before = Time.time;
        for (int i = 0; i < 1000; ++i)
        {
            for (int j = 0; j < objs.Length; ++j)
            {
                if (objs[j].CompareTag("Food"))
                    ++food;
                if (objs[j].CompareTag("Enemy"))
                    ++enemy;
            }
        }
        float delta = Time.time - before;
        return delta;
    }

    float RunComponents()
    {
        //int food = 0;
        //int enemy = 0;
        //float before = Time.time;
        //for (int i = 0; i < 1000; ++i)
        //{
        //    for (int j = 0; j < objs.Length; ++j)
        //    {
        //        if (objs[j].GetComponent<TFood>())
        //            ++food;
        //        if (objs[j].GetComponent<TEnemy>())
        //            ++enemy;
        //    }
        //}
        //float delta = Time.time - before;
        //return delta;
        return 0;
    }

}
