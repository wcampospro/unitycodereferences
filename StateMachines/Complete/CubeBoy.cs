﻿using UnityEngine.AI;
using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
public class CubeBoy : Unit
{
    public float foodRadius = 5;
    public LayerMask maskToSearch;
    internal Transform currentTarget = null;

    private StateMachine<CubeBoy> stateMachine = new StateMachine<CubeBoy>();
    private NavMeshAgent agent;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        stateMachine.ChangeState(new S_Idle(this, "Food"));
    }

    public void SetDestination(Vector3 dest)
    {
        agent.SetDestination(dest);
    }

    public void ChangeState(State<CubeBoy> newState)
    {
        stateMachine.ChangeState(newState);
    }

    private void Update()
    {
        stateMachine.UpdateState();
    }
}
